# ttrss-to-wp

WordPress Plugin zum Bloggen via TT-RSS

[Website](http://www.wlabs.de/7225/tschuess-wp-stats-willkommen-piwik-and-ttrss/)

## Beschreibung

Dies ist ein WordPress Plugin, mit dessen Hilfe einfache Links oder kurze Artikel direkt aus der TTRSS-Instanz heraus gebloggt werden können. Es unterstützt außerdem Tags und verwendet die Post-Formate "link" und "aside". ttrss-to-wp prüft regelmäßig einen RSS-Feed auf neue Einträge und erstellt zu jedem Eintrag einen neuen WordPress-Artikel.

## Vorraussetzungen

- [WordPress](http://wordpress.org/) ab 2.8
- [TT-RSS](http://tt-rss.org) ab 1.2.14

## Installation

**Hinweis:** Das Plugin erzeugt bei der Aktivierung in der Datenbank eine neue Tabelle mit dem Namen *wp_ttrss2wp_debug_log* (Präfix weicht möglicherweise ab), in der die Logs gespeichert werden. Diese Tabelle wird nach dem Deaktivieren des Plugins nicht automatisch gelöscht und muss bei Bedarf manuell entfernt werden.

### via GIT

1. Ins Plugin-Verzeichnis von WordPress wechseln, zB. ```$ cd wordpress/wp-content/plugins```
2. Code downloaden ```$ git clone git@github.com:Art4/ttrss-to-wp.git```
3. In WordPress das Plugin _TTRSS-to-WP_ aktivieren

### via S/FTP

1. [Code downloaden](https://github.com/Art4/ttrss-to-wp/archive/master.zip) und entpacken
2. Ordner _ttrss-to-wp-master_ umbenennen in _ttrss-to-wp_
3. Ordner in den Plugin-Ordner von WordPress kopieren (zB. _wordpress/wp-content/plugins_)
4. In WordPress das Plugin _TTRSS-to-WP_ aktivieren

## Einrichtung

Gehe im Adminbereich von WordPress zu _Einstellungen -> TTRSS-to-WP_. Dort gibt es die Reiter _Settings_ und _Debug_.

### Settings

**Hinweis:** Einige Einstellungen sind Überbleibsel aus dem Plugin RSS Digest und haben keine Funktion mehr.

ttrss-to-wp benötigt den RSS-Feed zu den [veröffentlichten Artikeln](http://tt-rss.org/redmine/projects/tt-rss/wiki/PublishArticles) deiner TT-RSS Instanz. Dieser muss bei **Feed** eingetragen werden. Natürlich funktioniert auch [jeder anderer RSS-Feed von TT-RSS](http://tt-rss.org/redmine/projects/tt-rss/wiki/GeneratedFeeds).

Wähle unter **Category for digest posts** aus, welcher Kategorie die erstellten WordPress-Artikel zugeordnet werden sollen.

Ein Klick auf den Button **Update TTRSS-to-WP Options** speichert die Einstellungen.

### Debug

Ganz unten bei _Debug Tools_ bietet das Plugin mit dem Button _Preview_ eine Vorschau über neuen Artikel im Feeds, die bei der nächsten automatischen Feed-Verarbeitug gebloggt werden würden. Mit dem Button _Post Now_ werden die gefundenen Artikel sofort gebloggt.

## Workflow

Dieser Abschnitt beschreibt, wie ich beim Lesen in der TT-RSS Instanz im Browser oder über die [Android-App](http://tt-rss.org/redmine/projects/tt-rss-android/wiki) interssante Artikel automatisch blogge. Dabei hat sich bei mir folgender Workflow etabliert:

Beim Lesen meiner Feeds über TT-RSS klicke ich bei interessanten Artikeln auf "[Veröffentlichen](http://tt-rss.org/redmine/projects/tt-rss/wiki/PublishArticles)". Das Plugin bloggt diese Artikel dann in meine WordPress-Kategorie "[Linksammlung](http://www.wlabs.de/blog/linksammlung/)". Das Post-Format der Artikel ist standardmäßig _link_. Das sieht dann zum Beispiel [so](http://www.wlabs.de/7847/die-woche-mit-open-source-unter-dem-radar/) aus.

TT-RSS bietet auch die Möglichkeit, eine kurze Artikelnotiz zu schreiben, die auch mit dem RSS-Feed ausgeliefert werden. Das Plugin erkennt diese Artikelnotiz und baut sie entsprechend im WordPress-Artikel ein. Das Post-Format des Artikels ändert sich dann zu _aside_. Das sieht dann zum Beispiel [so](http://www.wlabs.de/8006/linux-banking-trojaner-in-freier-wildbahn-gesichtet/) aus.

### Tags

Außerdem gibt es noch die Möglichkeit, dem WordPress-Artikel direkt Tags zuzuweisen. Dazu muss in der Artikelnotiz eine Zeile geschrieben werden, die mit "Tags: " beginnt. Die Tags werden kommasepariert dahinter geschrieben, also zum Beispiel: ```Tags: Tag1, nocheiner,undnocheiner```

Eine Artikelnotiz mit einem Text und Tags sieht dann in etwa so aus:

```
Hier ein Text, der im WordPress-Artikel über dem Link erscheinen wird.
Tags: Tag1, nocheiner,undnocheiner
```

Dabei ist egal, ob die Tags über oder unter dem Text stehen. Wichtig ist, dass sie in einer eigenen Zeile, die mit "Tags: " beginnt, stehen. Diese Zeile erscheint dann auch nicht im WordPress-Artikel.

## Sonstiges

### Atikel-Titel

Als Artikel-Titel in WordPress wird automatisch der Titel des Ursprungartikels vergeben. Das kann im Code [hier](https://github.com/Art4/ttrss-to-wp/blob/master/ttrss-to-wp.php#L288) bzw. [hier](https://github.com/Art4/ttrss-to-wp/blob/master/ttrss-to-wp.php#L284) angepasst werden.

### Artikel-Text

Der Artikel-Text wird nach folgendem Schema erstellt:

```
[optional: Artikel-Notiz]

<a href="[URL zum Ursprungsartikel]" rel="external">[Titel des Ursprungsartikels] - [www.domain-zum-artikel.com]</a>
```

Das kann im Code [hier](https://github.com/Art4/ttrss-to-wp/blob/master/ttrss-to-wp.php#L283) bzw. [hier](https://github.com/Art4/ttrss-to-wp/blob/master/ttrss-to-wp.php#L180) angepasst werden.

## Credits

Dieses Plugin basiert zum größen Teil auf dem Plugin [RSS Digest](http://wordpress.org/plugins/rss-digest/) von [Sam Charrington](http://sam.charrington.com/projects/rss-digest). Vielen Dank, Sam!
